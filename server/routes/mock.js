let mongoose = require('mongoose');

let Movie = require('../models/movie');
let User = require('../models/user');
let Rating = require('../models/rating');

async function mockUsers( ) {
  let users = 
    [ { email : 'khunt@uwlax.edu', nickname : 'The Dev', password : '123' },
      { email : 'jim123@acme.org', nickname : 'Mr. Jim', password : '123' },
      { email : 'abc@acme.org', nickname : 'AlphabetSoup', password : '123' } ];
  
  console.log( 'Before inserting', users );
  let saved = await User.insertMany( users );
  console.log( 'After inserting', saved);
  
}

async function mockMovies( cb ) {
  let movies = 
    [ { title: 'Avatar', year: '2009', rated: 'PG-13' },
    { title: 'I Am Legend', year: '2007', rated: 'PG-13' },
    { title: '300', year: '2006', rated: 'R' },
    { title: 'The Avengers', year: '2012', rated: 'PG-13' },
    { title: 'The Wolf of Wall Street', year: '2013', rated: 'R' },
    { title: 'Interstellar', year: '2014', rated: 'PG-13' },
    { title: 'Game of Thrones', year: '2011–', rated: 'TV-MA' },
    { title: 'Vikings', year: '2013–', rated: 'TV-14' },
    { title: 'Gotham', year: '2014–', rated: 'TV-14' },
    { title: 'Power', year: '2014–', rated: 'TV-MA' },
    { title: 'Narcos', year: '2015–', rated: 'TV-MA' },
    { title: 'Breaking Bad', year: '2008–2013', rated: 'TV-14' },
    { title: 'Doctor Strange', year: '2016', rated: 'NR' },
    { title: 'Rogue One: A Star Wars Story', year: '2016', rated: 'NR' },
    { title: 'Assassin\'s Creed', year: '2016', rated: 'NR' },
    { title: 'Luke Cage', year: '2016–', rated: 'TV-MA' } ]

  await Movie.insertMany( movies );
}

async function mockRatings(  ) {
  function randomRating() {
    return Math.floor( Math.random() * 10 ) + 1;
  }

  User.find({}, (err1, users) => {
    Movie.find({}, (err2, movies) => {
      users.forEach( user => {
        movies.filter( m => Math.random() < .25 ).forEach( movie => {
          let rating = new Rating( { userId : user._id, movieId : movie._id, rating : randomRating() } );
          rating.save();
        });
      })
    });
  });
}

async function mockData() {
  await mongoose.connection.dropDatabase();

  await mockUsers();
  await mockMovies();
  await mockRatings();
}

module.exports = mockData;