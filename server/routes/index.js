let express = require('express');
let router = express.Router();
let Movie = require('../models/movie');

router.get('/movies', function(req, res, next) {
  Movie.find({}, (err, movies) => {
    if( err ) {
      res.status(500).send("Error");
    } else {
      res.json( movies );
    }
  });
});

require('./mock')();

module.exports = router;
