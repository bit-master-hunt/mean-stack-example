const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema( {
    email : { type : String, unique : true},
    password : { type : String, required : true },
    nickname : { type : String, required : true },
    enabled : { type : Boolean, default : true },
    joined : { type : Date, default : Date.now }
});

const User = mongoose.model( 'User', UserSchema );

module.exports = User;