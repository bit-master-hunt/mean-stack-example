const mongoose = require('mongoose');

const MovieSchema = new mongoose.Schema( {
    title : { type : String, unique : true },
    year : { type : String, required : true },    
    rated : { type : String, enum : [ 'G', 'PG', 'PG-13', 'R', 'NC-17', 'TV-14', 'TV-MA', 'NR'], default : 'NR' },
});

const Movie = mongoose.model( 'Movie', MovieSchema );

module.exports = Movie;