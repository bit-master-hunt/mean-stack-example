const mongoose = require('mongoose');

const RatingSchema = new mongoose.Schema( {
    userId : { type: mongoose.Schema.ObjectId, ref: 'User' },
    movieId : { type: mongoose.Schema.ObjectId, ref: 'Movie' },
    rating : { type: Number, min: 1, max : 10 }
});

const Rating = mongoose.model( 'Rating', RatingSchema );

module.exports = Rating;
